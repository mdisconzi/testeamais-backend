<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;

Route::middleware('auth:sanctum')->group(function () {

    Route::name('students.')->prefix('students')->group(function () {
        Route::get('list',[StudentController::class, 'list'])->name('list');
        Route::post('store',[StudentController::class, 'store'])->name('store');
        Route::get('edit/{id}',[StudentController::class, 'edit'])->name('edit');
        Route::post('update',[StudentController::class, 'update'])->name('update');
        Route::post('destroy',[StudentController::class, 'destroy'])->name('destroy');
        //para não passar id de exclusão na url
    });

});


//api de geração do token de acesso
Route::middleware('api')
    ->post('/authenticate', [LoginController::class, 'authenticate']);
