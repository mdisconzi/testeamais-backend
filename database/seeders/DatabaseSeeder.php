<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        //cria um user de teste para acessar a api
        DB::table('users')->insert([
            'name' => "Zebedeu Alvarenga",
            'email' => 'teste@amais.com',
            'email_verified_at' =>  now(),
            'password' => Hash::make('amais2021'),
        ]);
    }        
}
