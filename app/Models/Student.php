<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;


class Student extends BaseModel
{
    use SoftDeletes;
    
    protected $fillable = [
        'name',
        'cpf',
        'email',
        'phone',
    ];

    protected $primaryKey = "ra"; //registro acadêmico

    protected $casts = [
        'phone' => 'array',
    ];
}
