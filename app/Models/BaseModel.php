<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

use App\Traits\Moldable;
use Illuminate\Support\Facades\Schema;

abstract class BaseModel extends Model
{
    use Moldable;

    protected $hidden = ['deleted_at',""];

    protected $rules = [];

    protected $dates = [
      'created_at',
      'updated_at',
    ];

    public function rules($name) {
        return $this->rules[$name] ?? [];
    }

    public function getPrimaryKey() {
      return $this->primaryKey;
    }

}