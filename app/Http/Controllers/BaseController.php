<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{
    protected $class = null;
    protected $customer   = null;

    public function __construct() {
      // $this->middleware('auth:admin');
      $strClass = $this->class;
      if(empty($this->class))
        $strClass = "App\\Models\\".str_replace('Controller',"",
        (new \ReflectionClass(get_class($this)))->getShortName()); 

      $this->class = class_exists($strClass) ? new $strClass : null;
    }

    public function edit($id) { 
      $c = $this->class::find($id);
      return response()->json([
        "success" => filled($c),
        "data" => $c
      ]);
    }
}