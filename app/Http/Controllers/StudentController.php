<?php

namespace App\Http\Controllers;
use App\Http\Controllers\BaseController;
use App\Http\Requests\StudentRequest;
use App\Models\Student;
use App\Http\Resources\StudentResource;
use Illuminate\Http\Request;

class StudentController extends BaseController
{

    public function store(StudentRequest $request){
        $data = (object) $request->all();
        $obj = new Student((array) $data);

        $res = $obj->save();

        return response()->json([
            "success"=>$res,
            "data"=>$obj
        ]);
    }

    public function update(StudentRequest $request){
        $new = (object) $request->all();
        $old = Student::find($new->updateId);
        $update = $old->update((array) $new);
        return response()->json([
            "success"=>$update,
            "data"=>$old
        ]);
    }
    
    public function list(){
        //esturar p processo de paginação posteriormente 
        // $students = Student::paginate(15);
        $students = Student::all();
        return StudentResource::collection($students);
    }
      
    public function edit($id){
        $student = Student::where('ra',$id)->first();
        return response()->json([
            "success"=>filled($student),
            "data"=>$student
        ]); 
    } 

    public function destroy(Request $request){
        $r = (object) $request->all();
        $student = Student::where('ra',$r->destroyId)->delete();
 
        return response()->json([
            "success"=>$student,
        ]);
    }     
}
