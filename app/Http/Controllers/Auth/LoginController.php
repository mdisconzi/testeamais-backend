<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function authenticate(Request $request) {
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);
    
        $attempt = Auth::attempt($credentials);
        $username = null;

        if ($attempt === true) {
            $request->session()->regenerate();
            $username = Auth::user()->name;
            //return redirect()->intended('dashboard');
        }

        return response()->json([
            "success" => $attempt,
            "username" => $username
        ]);
    }
}