<?php
namespace App\Http\Requests;

use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class StudentRequest extends BaseRequest
{

  protected function prepareForValidation() {
      $input = $this->all();
      $this->merge($input)
      ->merge([
      ]);
  }

  public function rules(){
    //
    $updateId = $this->input('updateId',null);
    $rules = [
      'name'=>'bail|required', 
      //poderia implementar um validador do cpf também, não feito por questão de tempo
      'cpf' => ['sometimes','bail','required',Rule::unique('students','cpf')->ignore($updateId)],
      'email'=>'required|email',
      //'phone'=>'',
    ];
    
    return collect($rules)
    ->all();
  }

  public function messages(){
    return [
      "cpf.unique" => "Este CPF já está cadastrado!",
    ];
  }
}

