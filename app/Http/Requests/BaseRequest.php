<?php
namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BaseRequest extends FormRequest
{
  public function rules() {
    return [];
  }

  public function messages() {
   return parent::messages();
  }
  
  // public function withValidator($validator) {
  //   $validator->after(function ($validator) {
  //     foreach($validator->errors()->keys() as $v){
  //       preg_match('/^(.+)\.([0-9]{1,})\.(.+)$/', $v, $match);
  //       if(filled($match)){
  //         $validator->errors()->merge([
  //           "$v" => json_encode([
  //             "id" => $this->input($match[1])[$match[2]]['id'],
  //             "multi" => $match[1],
  //             "index" => $match[2],
  //             "field" => $match[3]
  //           ])
  //         ]);
  //       }
  //     }
  //   });
  // }
}
