Comentários do processo de desenvolvimento
====
#### Marcelo Disconzi



### Arquitetura

- Conforme solicitado, o projeto foi criado utilizando Laravel e VueJS, os projetos foram divididos completamente, embora tenha por vezes trabalhado com aplicações Vue integradas no laravel, para o teste optei por separar completamente.

- Para o front criei uma estrutura de painel administrativo, com separação dos
serviços em módulos, com estrura de componentes bem divididas tanto para o layout quanto para os arquivos js empregados, incluindo persistencia com vuex com módulos escritos para permitir o reúso, isto é uma característica que busco implementar sempre, tendo otimizado muitos códigos que já havia escrito anteriormente.

- Essa estrutura pode pode exemplo ser usada para criar outros módulos facilmente, como financeiro, professores etc, cada um com seus serviços.

- O acesso ao app possui autenticação usando laravel Sanctum, via criação de tokens csrf, então com exção das rotas sessions (login,logou,404), todo rota no app passa por uma guard de autenticação, o mesmo vale para as consultas a API, com exceção da consulta inicial que faz a geração do token. as demais rotas do crud passam por um middlware do laravel sanctum, isso inclusive foi um dos motivos da demora na entrega, pois levou a problemas de CORS pela separação dos domínios.  

- Agora que estou tendo que colocar o serviço online, creio que nem havia a necessidade dessa autenticação, pois creio que será trabalhoso rodar isso nessas ferramentas como heroku e netlify, visto que são domains diferentes gerados.

### Lista de bibliotecas de terceiros utilizadas

- Sobre a utilizacão de libs de terceiros, utilizei algumas poucas, em especial para questões visuais, mas que poderiam ser substituídas por implementações nativas sem problema, outras libs utilizadas são muito robustas e creio que não sejam passíveis de problema, como axios, lodash, vue-router, vuex etc.

- axios
- vue-router
- dayjs (apenas para exibição de datas em formato humanizado na listagem)
- v-mask (máscara para o cpf)
- vue-sweetalert2 - (seria para exibição de alerta de confirmação estilizado, mas acabei optando por uma abordagem no próprio botão de delete, será removida)
- vue-toastification - (exibição de toasts/mensagens) mais estilizadas, embora o vuetify possua um componente semelhante o comportamente esperado acabaria levando muito tempo.
- vue-virtual-scroller - componente para renderização de listas em tempo de execução, utilizo ela em conjunto com um wrapper customizado da v-datatable.
- vuetify,
- vuex
- vuex-persistedstate (persistencia de estados para alguns states em módulos vuex, inclusive implementei os módulos vuex vcom componentização, e módulo padrão para ações de crud como neste projeto)

E outros pacotes são requisitos de desenvolvimento apenas.

- No Laravel são apenas o pacotes iniciais, como é só consulta de API, não foi necessário mais, nada. Inclusive daria para testar se o Lumen atenderia, mas só percebi depois de iniciar e não daria mais tempo.


### O que você melhoraria se tivesse mais tempo 

- Terminaria as ações para gestão de usuários, criação, revogação de chaves, validação de contas etc.
- melhoraria a questão visual, em especial da tela de login e de outras sessions que apenas gerei para não haver quebra, mas sem tempo para ajustes visuais.
- faria a refatoração de alguns códigos que percebi que podem ser otimizados, em especial alguns mixins que utilizo para cascatear para os componentes de cima $emits.
- Vários outros items, sempre há coisas a se melhorar.

- Todos os requisitos foram atendidos, apenas com um pequeno ajuste no campo RA,
como se trata de chave única, o transformei na chave primária da tabela com 
autoincrement mas com o mesmo nome RA.

- Logo, o campo não editável exemplificado é o cpf, incluindo as validações de 
atendimento ao algorítimo no front, e verificação de unicidade na request.

### Quais requisitos obrigatórios que não foram entregues

Acredito que todos tenham sido