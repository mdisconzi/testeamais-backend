## API com laravel 8 e authenticação usando Sanctum para um CRUD

### Instruções de instalação do servidor da API

#### requisitos

- PHP ^7.3|^8.0
- Laravel ^8.65


### Faça um clone do repositório, e depois a sequência abaixo:

```sh
composer install
```

- Faça as configurações no .env

configure os parametros DB_ 


- Gerar a chave do app
```sh
php artisan key:generate
```

- Para atualizar/armazenar o cache das configs

```sh
php artisan config:cache
```

- Criação das tabelas, apenas as tabelas básicas do laravel e a tabela de Students

```sh
php artisan migrate
```

- Criação do user de teste para comunicação com a API

```sh
php artisan db:seed
```
será criado o usuário abaixo:

email: teste@amais.com
senha: amais2021


### inicie o server do projeto com php artisan serve, ou configure um domínio local com apache virtual host, devido a questão de CORS, pode ser que a autenticação seja bloqueada, para testar localmente, crie os servidores com hosts virtuais e utilize um como domínio e outro como subdomínio, ex:

- Servidor da API
```sh
Servidor da API: https://api.testeamais.com
```

- Servidor do front Vue
```sh
Servidor da API: https://testeamais.com
```

### Após rodar os projetos localmente, será necessário alterar as variáveis de ambiente no Laravel e Vue, como segue:


.ENV no Laravel, altere a variável que altera a session config
```sh
SESSION_DOMAIN = testeamais.com
``

-.ENV no Vue
```sh
VUE_APP_API = http://api.testeamais.com
VUE_APP_API_API  = http://api.testeamais.com/api
```


### Consumo da API:

- As rotas para ações são todas protegidas pelo midleware do Laravel Sanctum, e precisam ser acessadas informando um
token que é gerado na primeira autenticação e repassada nas requisições seguintes, segue abaixo as rotas:


### API Blueprint

## GET /sanctum/csrf-cookie
+ Geração do token csrf


## POST /api/authenticate
+ Authenticação do usuário

+ Parâmetros

    + email 
    + password 

+ o valor de retorno será um booleano


